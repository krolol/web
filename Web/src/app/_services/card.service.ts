import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Card} from '../Modules/card';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CardService {

  constructor(private http: HttpClient) { }

  addCard(card: Card) {
    return this.http.post(`${environment.appUrl}/api/cards/add`, card);
  }

  deleteCard(id: string) {
    return this.http.delete(`${environment.appUrl}/api/cards/${id}`);
  }

  getCards() {
    return this.http.get<Card[]>(`${environment.appUrl}/api/cards`);
  }
}
