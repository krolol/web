import { Injectable } from '@angular/core';
import {Image} from '../Modules/image';
import {Room} from '../Modules/room';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Filter} from '../Modules/filter';
import {Book} from '../Modules/book';

@Injectable({
  providedIn: 'root'
})
export class RoomService {

  constructor(private http: HttpClient) { }

  addRoom(room: Room, images: Image[]){
    room.images = images;
    return this.http.post(`${environment.appUrl}/api/rooms/create`, room);
  }

  getRoom(id: string) {
    return this.http.get<any>(`${environment.appUrl}/api/rooms/${id}`);
  }

  getRooms(id: string) {
    return this.http.get<any>(`${environment.appUrl}/api/rooms/hotels/${id}`);
  }

  deleteRoom(id: string) {
    return this.http.delete(`${environment.appUrl}/api/rooms/${id}`);
  }

  updateRoom(room: Room, images: Image[]) {
    room.images = images;
    return this.http.put(`${environment.appUrl}/api/rooms/update`, room);
  }

  booking(book: Book) {
    const books: Book[] = [book];
    return this.http.post(`${environment.appUrl}/api/rooms/booking`, books);
  }

  getBookings() {
    return this.http.get(`${environment.appUrl}/api/rooms/bookings`);
  }
}
