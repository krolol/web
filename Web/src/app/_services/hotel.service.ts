import { Injectable } from '@angular/core';
import { environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {HotelResult} from '../Modules/hotel-result';
import {Hotel} from '../Modules/hotel';
import {Image} from '../Modules/image';
import {Filter} from '../Modules/filter';
import {Report} from '../Modules/report';
import {ReportResult} from '../Modules/report-result';


@Injectable({
  providedIn: 'root'
})
export class HotelService {

  constructor(private http: HttpClient) { }

  getUserHotels() {
    return this.http.get<HotelResult[]>(`${environment.appUrl}/api/hotels/own`);
  }

  deleteHotel(id: string) {
    return this.http.delete(`${environment.appUrl}/api/hotels/${id}`);
  }

  getHotel(id: string) {
    return this.http.get<HotelResult>(`${environment.appUrl}/api/hotels/${id}`);
  }

  updateHotel(hotel: Hotel, images: Image[]) {
    hotel.images = images;
    return this.http.put(`${environment.appUrl}/api/hotels/update`, hotel);
  }

  addHotel(hotel: Hotel, images: Image[]) {
    hotel.images = images;
    return this.http.post<any>(`${environment.appUrl}/api/hotels/create`, hotel);
  }

  getAllHotels() {
    return this.http.get<HotelResult[]>(`${environment.appUrl}/api/hotels`);
  }

  getHotelsByFilter(filter: Filter) {
    filter.dateFrom = new Date(filter.dateFrom).toISOString();
    filter.dateTo = new Date(filter.dateTo).toISOString();
    filter.adults = parseInt(filter.adults.toString(), 10);
    filter.children = parseInt(filter.children.toString(), 10);
    filter.roomsCount = parseInt(filter.roomsCount.toString(), 10);
    return this.http.post<HotelResult[]>(`${environment.appUrl}/api/hotels/filter`, filter);
  }

  addReport(report: Report) {
    return this.http.post(`${environment.appUrl}/api/hotels/comments/add`, report);
  }

  getReports(id: string) {
    return this.http.get<ReportResult[]>(`${environment.appUrl}/api/hotels/comments/${id}`);
  }
}
