import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';
import {User} from '../Modules/user';
import {UpdatePassword} from '../Modules/update-password';
import {Card} from '../Modules/card';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  login(username: string, password: string) {
    return this.http.post<any>(`${environment.appUrl}/api/accounts/token`,
      { username: username, password: password })
      .pipe(map(user => {
        // login successful if there's a jwt token in the response
        if (user && user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
        }

        return user;
      }));
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
  }

  register(user: User) {
    user.birthdayUtc = new Date(user.birthdayUtc).toISOString();
    if (user.roleToAdd.toString() === 'Tenant') {
      user.roleToAdd = 1;
    } else {
      user.roleToAdd = 2;
    }
    return this.http.post(`${environment.appUrl}/api/accounts/register`, user);
  }

  update(user: User) {
    user.birthdayUtc = new Date(user.birthdayUtc).toISOString();
    return this.http.put<User>(`${environment.appUrl}/api/accounts/update`, user);
  }

  updatePassword(old: string, newP: string) {
    const pass = new UpdatePassword();
    pass.oldPassword = old;
    pass.newPassword = newP;
    return this.http.put(`${environment.appUrl}/api/accounts/update-password`, pass);
  }

  withDraw(card: Card) {
    return this.http.post(`${environment.appUrl}/api/accounts/withdraw-money`, card);
  }

  topUp(card: Card) {
    return this.http.post(`${environment.appUrl}/api/accounts/top-up-balance`, card);
  }
}
