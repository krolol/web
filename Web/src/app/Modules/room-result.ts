import {AzureImage} from './azure-image';

export class RoomResult {
  id: string;
  hotelId: string;
  title: string;
  price: number;
  peopleCount: number;
  description: string;
  azureImages: AzureImage[];
}
