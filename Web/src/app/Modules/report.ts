export class Report {
  id: string;
  rating: number;
  pluses: string;
  minuses: string;
  hotelId: string;
}
