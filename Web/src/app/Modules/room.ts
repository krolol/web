import {Image} from './image';

export class Room {
  id: string;
  hotelId: string;
  title: string;
  price: number;
  peopleCount: number;
  description: string;
  images: Image[];
}
