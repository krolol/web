export class User {
  id: string;
  name: string;
  surName: string;
  userName: string;
  password: string;
  birthdayUtc: string;
  email: string;
  phoneNumber: string;
  roleToAdd: number;
  balance: number;
}
