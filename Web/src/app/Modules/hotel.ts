import { Image } from './image'

export class Hotel {
  title: string;
  hotelType:	string;
  addressCode:	string;
  description:	string;
  images:	Image[];
}
