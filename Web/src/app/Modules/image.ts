export class Image {
  fileName?: string;
  fileType?: string;
  data?: string;
  image?: string;
}
