export class Filter {
  city: string;
  roomsCount: number;
  adults: number;
  children: number;
  dateFrom: string;
  dateTo: string;
}
