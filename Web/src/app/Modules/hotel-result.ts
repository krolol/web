import {AzureImage} from './azure-image';

export class HotelResult {
  id: string;
  title: string;
  hotelType: string;
  addressCode: string;
  description: string;
  rating: number;
  minPrice: number;
  azureImages: AzureImage[];
}
