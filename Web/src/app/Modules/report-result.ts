export class ReportResult {
  id: string;
  rating: number;
  pluses: string;
  minuses: string;
  hotelId: string;
  userName: string;
}
