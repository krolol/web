export class Book {
  room: string;
  dateFrom: string;
  dateTo: string;
  adults: number;
  children: number;
}
