import { Routes, RouterModule } from '@angular/router';
import { RegistrationComponent } from './components/registration/registration.component';
import { LoginComponent } from './components/login/login.component';
import {ProfileComponent} from './components/profile/profile.component';
import {AuthGuard} from './_guard/auth.guard';
import {HotelsControlComponent} from './components/hotels-control/hotels-control.component';
import {CardsControlComponent} from './components/cards-control/cards-control.component';
import {HotelControlInfoComponent} from './components/hotel-control-info/hotel-control-info.component';
import {AddRoomComponent} from './components/add-room/add-room.component';
import {AddHotelComponent} from './components/add-hotel/add-hotel.component';
import {AddCardComponent} from './components/add-card/add-card.component';
import {RoomsControlComponent} from './components/rooms-control/rooms-control.component';
import {RoomControlInfoComponent} from './components/room-control-info/room-control-info.component';
import {HomeComponent} from './components/home/home.component';
import {ChangePasswordComponent} from './components/change-password/change-password.component';
import {OpeartionCardComponent} from './components/opeartion-card/opeartion-card.component';
import {ReportComponent} from './components/report/report.component';
import {SearchHotelComponent} from './components/search-hotel/search-hotel.component';
import {HotelComponent} from './components/hotel/hotel.component';
import {RoomsComponent} from './components/rooms/rooms.component';
import {BookingsComponent} from './components/bookings/bookings.component';

const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegistrationComponent },
  { path: 'hotels', component: HotelsControlComponent, canActivate: [AuthGuard]},
  { path: 'cards', component: CardsControlComponent, canActivate: [AuthGuard]},
  { path: 'addHotel', component: AddHotelComponent, canActivate: [AuthGuard]},
  { path: 'addCard', component: AddCardComponent, canActivate: [AuthGuard]},
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard]},
  { path: 'hotel/:id', component: HotelComponent},
  { path: 'cards', component: CardsControlComponent, canActivate: [AuthGuard]},
  { path: 'changePassword', component: ChangePasswordComponent, canActivate: [AuthGuard]},
  { path: 'hotelcontrol/:hotelId', component: HotelControlInfoComponent, canActivate: [AuthGuard]},
  { path: 'cardOperation/:operation', component: OpeartionCardComponent, canActivate: [AuthGuard]},
  { path: 'hotelReport/:id', component: ReportComponent, canActivate: [AuthGuard]},
  { path: 'search/:city/:roomsCount/:adults/:children/:dateFrom/:dateTo', component: SearchHotelComponent},
  { path: 'addRoom/:hotelId', component: AddRoomComponent, canActivate: [AuthGuard]},
  { path: 'rooms/:hotelId', component: RoomsControlComponent, canActivate: [AuthGuard]},
  { path: 'roomControl/:hotelId/:roomId', component: RoomControlInfoComponent, canActivate: [AuthGuard]},
  { path: 'hotelRooms/:id', component: RoomsComponent},
  { path: 'bookings', component: BookingsComponent, canActivate: [AuthGuard]},

  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);
