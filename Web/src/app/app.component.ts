/* tslint:disable */
import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AlertService} from './_services/alert.service';
import {UserService} from './_services/user.service';
import {User} from './Modules/user';

@Component({
  selector: 'app',
  templateUrl: 'app.component.html'
})

export class AppComponent implements OnInit {
  currentUser: User;
  login = false;

  constructor(
    private userService: UserService,
    private router: Router) {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  logout() {
    this.userService.logout();
    window.location.reload();
  }

  topUp() {
    this.router.navigate(['/cardOperation/topUp']);
  }

  withDraw() {
    this.router.navigate(['cardOperation/withDraw']);
  }

  ngOnInit(): void {
    if (this.currentUser != null) {
      this.login = true;
    }
  }
}
