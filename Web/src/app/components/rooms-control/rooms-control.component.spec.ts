import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomsControlComponent } from './rooms-control.component';

describe('RoomsControlComponent', () => {
  let component: RoomsControlComponent;
  let fixture: ComponentFixture<RoomsControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoomsControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomsControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
