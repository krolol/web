import { Component, OnInit } from '@angular/core';
import {AlertService} from '../../_services/alert.service';
import {ActivatedRoute, Router} from '@angular/router';
import {RoomService} from '../../_services/room.service';
import {first} from 'rxjs/operators';
import {RoomResult} from '../../Modules/room-result';

@Component({
  selector: 'app-rooms-control',
  templateUrl: './rooms-control.component.html',
  styleUrls: ['./rooms-control.component.css']
})
export class RoomsControlComponent implements OnInit {
  private sub: any;
  private id: string;
  private rooms: RoomResult[];

  constructor(private roomService: RoomService,
              private alertService: AlertService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = params['hotelId'];
    });
    this.getRooms(this.id);
  }

  getRooms(id: string) {
    this.roomService.getRooms(id).pipe(first()).subscribe(rooms => {
      this.rooms = rooms;
    });
  }

  changeRoomInfo(id: string) {
    this.router.navigate([`/roomControl/${id}/${this.id}`]);
  }

  deleteRoom(id: string) {
    this.roomService.deleteRoom(id)
      .pipe(first())
      .subscribe(
        () => {
          this.alertService.success('Delete successful', true);
          window.location.reload();
        },
        error => {
          this.alertService.error(error);
        });
  }
}
