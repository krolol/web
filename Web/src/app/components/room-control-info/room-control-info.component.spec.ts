import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomControlInfoComponent } from './room-control-info.component';

describe('RoomControlInfoComponent', () => {
  let component: RoomControlInfoComponent;
  let fixture: ComponentFixture<RoomControlInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoomControlInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomControlInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
