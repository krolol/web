import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {RoomService} from '../../_services/room.service';
import {first} from 'rxjs/operators';
import {RoomResult} from '../../Modules/room-result';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertService} from '../../_services/alert.service';
import {UploadMetadata} from 'angular2-image-upload';
import {Image} from '../../Modules/image';

@Component({
  selector: 'app-room-control-info',
  templateUrl: './room-control-info.component.html',
  styleUrls: ['./room-control-info.component.css']
})
export class RoomControlInfoComponent implements OnInit {
  private sub: any;
  private id: string;
  private roomId: string;
  roomForm: FormGroup;
  private room: RoomResult;
  private loading = false;
  private submitted = false;

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private alertService: AlertService,
              private roomService: RoomService,) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = params['hotelId'];
      this.roomId = params['roomId'];
    });
    this.getRoom();
    this.roomForm = this.formBuilder.group({
      id: [this.roomId],
      hotelId: [this.id],
      title: ['', Validators.required],
      price: ['', Validators.required],
      peopleCount: ['', Validators.required],
      description: ['', Validators.required],
      images: [[]]
    });
  }

  getRoom() {
    this.roomService.getRoom(this.id).pipe(first()).subscribe(room => {
      this.room = room;
    });
  }

  onBeforeUpload = (metadata: UploadMetadata) => {
    const file = metadata.file;
    const images = this.roomForm.controls['images'].value;
    images.push(file);
    this.roomForm.controls['images'].setValue(images);
    return metadata;
  };

  async convertFilesToIImageDTOAsync(files: File[]) {
    const images: Image[] = [];
    for (const file of files) {
      const image = await this.convertFileToIImageDTOAsync(file);
      images.push(image);
    }

    return images;
  }

  async convertFileToIImageDTOAsync(file: File): Promise<Image> {
    return new Promise<Image>((resolve) => {
      const reader: FileReader = new FileReader();
      reader.onload = () => {
        const image: Image = {
          fileName: file.name,
          fileType: file.type,
          data: reader.result.toString().split(',')[1]
        };

        resolve(image);
      };

      reader.readAsDataURL(file);
    });
  }

  async onSubmit() {
    const files: File[] = this.roomForm.controls['images'].value;
    const images: Image[] = await this.convertFilesToIImageDTOAsync(files);
    this.submitted = true;

    // stop here if form is invalid
    if (this.roomForm.invalid) {
      return;
    }
    this.loading = true;
    this.roomService.updateRoom(this.roomForm.value, images)
      .pipe(first())
      .subscribe(
        () => {
          this.alertService.success('Hotel updated successful', true);
          this.router.navigate(['']);
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }
}
