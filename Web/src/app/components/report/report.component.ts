import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AlertService} from '../../_services/alert.service';
import {HotelService} from '../../_services/hotel.service';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {
  reportForm: FormGroup;
  id: string;
  sub: any;
  loading = false;
  submitted = false;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private route: ActivatedRoute,
              private alertService: AlertService,
              private hotelService: HotelService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
    });
    this.reportForm = this.formBuilder.group({
      rating: ['', Validators.required],
      pluses: [''],
      minuses: [''],
      hotelId: [this.id]
    });
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.reportForm.invalid) {
      return;
    }

    this.loading = true;
    this.hotelService.addReport(this.reportForm.value)
      .pipe(first())
      .subscribe(
        () => {
          this.alertService.success('Report added successful', true);
          this.router.navigate([`/hotel/${this.id}`]);
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }

}
