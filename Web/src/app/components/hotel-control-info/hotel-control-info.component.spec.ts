import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HotelControlInfoComponent } from './hotel-control-info.component';

describe('HotelControlInfoComponent', () => {
  let component: HotelControlInfoComponent;
  let fixture: ComponentFixture<HotelControlInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HotelControlInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotelControlInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
