import { Component, OnInit } from '@angular/core';
import {HotelService} from '../../_services/hotel.service';
import {AlertService} from '../../_services/alert.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HotelResult} from '../../Modules/hotel-result';
import {first} from 'rxjs/operators';
import {FileHolder, UploadMetadata} from 'angular2-image-upload';
import {Image} from '../../Modules/image';

@Component({
  selector: 'app-hotel-control-info',
  templateUrl: './hotel-control-info.component.html',
  styleUrls: ['./hotel-control-info.component.css']
})
export class HotelControlInfoComponent implements OnInit {
  hotelForm: FormGroup;
  submitted = false;
  id: string;
  loading = false;
  private sub: any;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private alertService: AlertService,
    private hotelService: HotelService) {
  }

  ngOnInit() {
    this.init();
  }

  init() {
    this.sub = this.route.params.subscribe(params => {
      this.id = params['hotelId'];
    });
    this.hotelForm = this.formBuilder.group({
      id: [this.id],
      title: ['', Validators.required],
      addressCode: ['', Validators.required],
      description: ['', Validators.required],
      images: [[], Validators.required],
      hotelType: ['Hotel', Validators.required]
    });
  }

  onBeforeUpload = (metadata: UploadMetadata) => {
    const file = metadata.file;
    const images = this.hotelForm.controls['images'].value;
    images.push(file);
    this.hotelForm.controls['images'].setValue(images);
    return metadata;
  }

  async convertFilesToIImageDTOAsync(files: File[]) {
    const images: Image[] = [];
    for (const file of files) {
      const image = await this.convertFileToIImageDTOAsync(file);
      images.push(image);
    }

    return images;
  }

  async convertFileToIImageDTOAsync(file: File): Promise<Image> {
    return new Promise<Image>((resolve) => {
      const reader: FileReader = new FileReader();
      reader.onload = () => {
        const image: Image = {
          fileName: file.name,
          fileType: file.type,
          data: reader.result.toString().split(',')[1]
        };

        resolve(image);
      };

      reader.readAsDataURL(file);
    });
  }

  onRemoved(fileHolder: FileHolder): void {
    const file = fileHolder.file;
    const files: File[] = this.hotelForm.controls['images'].value;
    const index = files.findIndex(x => x.name === file.name);
    if (index !== -1) {
      files.splice(index, 1);
      this.hotelForm.controls['images'].setValue(files);
    }
  }

  async onSubmit() {
    const files: File[] = this.hotelForm.controls['images'].value;
    const images: Image[] = await this.convertFilesToIImageDTOAsync(files);
    this.submitted = true;

    // stop here if form is invalid
    if (this.hotelForm.invalid) {
      return;
    }
    this.loading = true;
    this.hotelService.updateHotel(this.hotelForm.value, images)
      .pipe(first())
      .subscribe(
        () => {
          this.alertService.success('Hotel updated successful', true);
          this.router.navigate(['/profile']);
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }
}
