import { Component, OnInit } from '@angular/core';
import {first} from 'rxjs/operators';
import {Image} from '../../Modules/image';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertService} from '../../_services/alert.service';
import {HotelService} from '../../_services/hotel.service';
import {Router} from '@angular/router';
import {FileHolder, UploadMetadata} from 'angular2-image-upload';

@Component({
  selector: 'app-add-hotel',
  templateUrl: './add-hotel.component.html',
  styleUrls: ['./add-hotel.component.css']
})
export class AddHotelComponent implements OnInit {

  addHotelForm: FormGroup;
  loading = false;
  submitted = false;
  imageUploadStyle = {
    selectButton: {
      'background-color': '#28a745',
      'border-color': '#28a745',
      'border-top-left-radius': '.25rem',
      'border-bottom-left-radius': '.25rem',
    },
    clearButton: {
      'background-color': '#dc3545',
      'border-color': '#dc3545',
      'border-top-right-radius': '.25rem',
      'border-bottom-right-radius': '.25rem',
    },
  };

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private hotelService: HotelService,
    private alertService: AlertService) { }

  ngOnInit() {
    this.addHotelForm = this.formBuilder.group({
      title: ['', Validators.required],
      hotelType: ['', Validators.required],
      addressCode: ['', Validators.required],
      description: '',
      images: [[], Validators.required],
    });
  }

  onBeforeUpload = (metadata: UploadMetadata) => {
    const file = metadata.file;
    const images = this.addHotelForm.controls['images'].value;
    images.push(file);
    this.addHotelForm.controls['images'].setValue(images);
    return metadata;
  }

  async convertFilesToIImageDTOAsync(files: File[]) {
    const images: Image[] = [];
    for (const file of files) {
      const image = await this.convertFileToIImageDTOAsync(file);
      images.push(image);
    }

    return images;
  }

  async convertFileToIImageDTOAsync(file: File): Promise<Image> {
    return new Promise<Image>((resolve) => {
      const reader: FileReader = new FileReader();
      reader.onload = () => {
        const image: Image = {
          fileName: file.name,
          fileType: file.type,
          data: reader.result.toString().split(',')[1]
        };

        resolve(image);
      };

      reader.readAsDataURL(file);
    });
  }

  onRemoved(fileHolder: FileHolder): void {
    const file = fileHolder.file;
    const files: File[] = this.addHotelForm.controls['images'].value;
    const index = files.findIndex(x => x.name === file.name);
    if (index !== -1) {
      files.splice(index, 1);
      this.addHotelForm.controls['images'].setValue(files);
    }
  }
  get f() { return this.addHotelForm.controls; }

  async onSubmit() {
    const files: File[] = this.addHotelForm.controls['images'].value;
    const images: Image[] = await this.convertFilesToIImageDTOAsync(files);
    this.submitted = true;

    if (this.addHotelForm.invalid) {
      return;
    }

    this.loading = true;
    this.hotelService.addHotel(this.addHotelForm.value, images)
      .pipe(first())
      .subscribe(
        () => {
          this.alertService.success('Add hotel successful', true);
          this.router.navigate(['']);
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }
}
