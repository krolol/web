import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertService} from '../../_services/alert.service';
import {CardService} from '../../_services/card.service';
import {Router} from '@angular/router';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-add-card',
  templateUrl: './add-card.component.html',
  styleUrls: ['./add-card.component.css']
})
export class AddCardComponent implements OnInit {
  addCardForm: FormGroup;
  loading = false;
  submitted = false;
  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private cardService: CardService,
              private alertService: AlertService) { }

  ngOnInit() {
    this.addCardForm = this.formBuilder.group({
      cardNumber: ['', Validators.required]
    });
  }
  get f() { return this.addCardForm.controls; }

  async onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.addCardForm.invalid) {
      return;
    }

    this.loading = true;
    this.cardService.addCard(this.addCardForm.value)
      .pipe(first())
      .subscribe(
        () => {
          this.alertService.success('Add Card successful', true);
          this.router.navigate(['/profile']);
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }
}
