import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpeartionCardComponent } from './opeartion-card.component';

describe('OpeartionCardComponent', () => {
  let component: OpeartionCardComponent;
  let fixture: ComponentFixture<OpeartionCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpeartionCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpeartionCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
