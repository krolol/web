import { Component, OnInit } from '@angular/core';
import {first} from 'rxjs/operators';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../_services/user.service';
import {AlertService} from '../../_services/alert.service';
import {Card} from '../../Modules/card';
import {CardService} from '../../_services/card.service';
import {User} from '../../Modules/user';


@Component({
  selector: 'app-opeartion-card',
  templateUrl: './opeartion-card.component.html',
  styleUrls: ['./opeartion-card.component.css']
})
export class OpeartionCardComponent implements OnInit {
  cardForm: FormGroup;
  loading = false;
  submitted = false;
  operation: string;
  cards: Card[];
  currentUser: User;
  sub: any;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private userService: UserService,
              private alertService: AlertService,
              private cardService: CardService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.getCards();
    this.sub = this.route.params.subscribe(params => {
      this.operation = params['operation'];
    });
    this.cardForm = this.formBuilder.group({
      cardId: ['', Validators.required],
      amount: ['', Validators.required]
    });
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  getCards() {
    this.cardService.getCards().pipe(first()).subscribe(cards => {
      this.cards = cards;
    });
  }

  get f() { return this.cardForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.cardForm.invalid) {
      return;
    }

    this.loading = true;
    if (this.operation === "withDraw") {
      this.userService.withDraw(this.cardForm.value)
        .pipe(first())
        .subscribe(
          () => {
            this.currentUser.balance -= this.cardForm.value.amount;
            localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
            this.alertService.success('Operation successful', true);
            this.router.navigate(['']);
            window.location.reload();
          },
          error => {
            this.alertService.error(error);
            this.loading = false;
          });
    } else {
      this.userService.topUp(this.cardForm.value)
        .pipe(first())
        .subscribe(
          () => {
            this.currentUser.balance += this.cardForm.value.amount;
            localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
            this.alertService.success('Operation successful', true);
            this.router.navigate(['']);
            window.location.reload();
          },
          error => {
            this.alertService.error(error);
            this.loading = false;
          });
    }
  }
}
