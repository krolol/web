import { Component, OnInit } from '@angular/core';
import {first} from 'rxjs/operators';
import {Image} from '../../Modules/image';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertService} from '../../_services/alert.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FileHolder, UploadMetadata} from 'angular2-image-upload';
import {RoomService} from '../../_services/room.service';

@Component({
  selector: 'app-add-room',
  templateUrl: './add-room.component.html',
  styleUrls: ['./add-room.component.css']
})
export class AddRoomComponent implements OnInit {
  addRoomForm: FormGroup;
  loading = false;
  submitted = false;
  imageUploadStyle = {
    selectButton: {
      'background-color': '#28a745',
      'border-color': '#28a745',
      'border-top-left-radius': '.25rem',
      'border-bottom-left-radius': '.25rem',
    },
    clearButton: {
      'background-color': '#dc3545',
      'border-color': '#dc3545',
      'border-top-right-radius': '.25rem',
      'border-bottom-right-radius': '.25rem',
    },
  };
  sub: any;
  id: string;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private roomService: RoomService,
    private alertService: AlertService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = params['hotelId'];
    });
    this.addRoomForm = this.formBuilder.group({
      hotelId: [this.id],
      title: ['', Validators.required],
      price: ['', Validators.required],
      peopleCount: ['', Validators.required],
      description: '',
      images: [[], Validators.required],
    });
  }
  onBeforeUpload = (metadata: UploadMetadata) => {
    const file = metadata.file;
    const images = this.addRoomForm.controls['images'].value;
    images.push(file);
    this.addRoomForm.controls['images'].setValue(images);
    return metadata;
  };

  async convertFilesToIImageDTOAsync(files: File[]) {
    const images: Image[] = [];
    for (const file of files) {
      const image = await this.convertFileToIImageDTOAsync(file);
      images.push(image);
    }

    return images;
  }

  async convertFileToIImageDTOAsync(file: File): Promise<Image> {
    return new Promise<Image>((resolve) => {
      const reader: FileReader = new FileReader();
      reader.onload = () => {
        const image: Image = {
          fileName: file.name,
          fileType: file.type,
          data: reader.result.toString().split(',')[1]
        };

        resolve(image);
      };

      reader.readAsDataURL(file);
    });
  }

  onRemoved(fileHolder: FileHolder): void {
    const file = fileHolder.file;
    const files: File[] = this.addRoomForm.controls['images'].value;
    const index = files.findIndex(x => x.name === file.name);
    if (index !== -1) {
      files.splice(index, 1);
      this.addRoomForm.controls['images'].setValue(files);
    }
  }
  get f() { return this.addRoomForm.controls; }

  async onSubmit() {
    const files: File[] = this.addRoomForm.controls['images'].value;
    const images: Image[] = await this.convertFilesToIImageDTOAsync(files);
    this.submitted = true;

    // stop here if form is invalid
    if (this.addRoomForm.invalid) {
      return;
    }

    this.loading = true;
    this.roomService.addRoom(this.addRoomForm.value, images)
      .pipe(first())
      .subscribe(
        () => {
          this.alertService.success('Add room successful', true);
          this.router.navigate(['/profile']);
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }
}
