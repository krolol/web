import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../_services/user.service';
import {first} from 'rxjs/operators';
import {AlertService} from '../../_services/alert.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  changePassword: FormGroup;
  private loading: boolean;

  constructor(private formBuilder: FormBuilder,
              private userService: UserService,
              private alertService: AlertService,
              private router: Router) {
  }
  checkPasswords(group: FormGroup) {
    let pass = group.controls.newPassword.value;
    let confirmPass = group.controls.repeatPassword.value;

    return pass === confirmPass ? null : { repeatError: true }
  }

  ngOnInit() {
    this.changePassword = this.formBuilder.group({
      oldPassword: ['', Validators.required],
      newPassword: ['', [Validators.required, Validators.minLength(8)]],
      repeatPassword: ['', Validators.required]
    }, {validators: this.checkPasswords});
  }

  onSubmit() {
    let old = this.changePassword.value.oldPassword;
    let newPas = this.changePassword.value.newPassword;
    this.userService.updatePassword(old, newPas)
      .pipe(first())
      .subscribe(
        () => {
          this.alertService.success('Password changed successful', true);
          this.router.navigate(['/profile']);
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }
}
