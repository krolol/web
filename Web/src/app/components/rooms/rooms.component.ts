import { Component, OnInit } from '@angular/core';
import {first} from 'rxjs/operators';
import {RoomService} from '../../_services/room.service';
import {AlertService} from '../../_services/alert.service';
import {ActivatedRoute, Router} from '@angular/router';
import {RoomResult} from '../../Modules/room-result';
import {environment} from '../../../environments/environment';
import {Book} from '../../Modules/book';

@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.css']
})
export class RoomsComponent implements OnInit {
  private sub: any;
  private id: string;
  private currentUser = false;
  private rooms: RoomResult[];

  constructor(private roomService: RoomService,
              private alertService: AlertService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    if (localStorage.getItem('currentUser') !== null)
      this.currentUser = true;
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
    });
    this.getRooms(this.id);
  }

  booking(id: string) {
    const book = new Book();
    book.adults = environment.filter.adults;
    book.children = environment.filter.children;
    book.dateFrom = environment.filter.dateFrom;
    book.dateTo = environment.filter.dateTo;
    book.room = id;
    debugger;
    this.roomService.booking(book)
      .pipe(first())
      .subscribe(
        () => {
          this.alertService.success('Booking successful', true);
          window.location.reload();
        },
        error => {
          this.alertService.error('Not for you');
        });

  }

  getRooms(id: string) {
    this.roomService.getRooms(id).pipe(first()).subscribe(rooms => {
      this.rooms = rooms;
    });
  }

}
