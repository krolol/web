import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HotelsControlComponent } from './hotels-control.component';

describe('HotelsControlComponent', () => {
  let component: HotelsControlComponent;
  let fixture: ComponentFixture<HotelsControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HotelsControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotelsControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
