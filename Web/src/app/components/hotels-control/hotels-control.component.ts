import { Component, OnInit } from '@angular/core';
import {first} from 'rxjs/operators';
import {HotelResult} from '../../Modules/hotel-result';
import {HotelService} from '../../_services/hotel.service';
import {AlertService} from '../../_services/alert.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-hotels-control',
  templateUrl: './hotels-control.component.html',
  styleUrls: ['./hotels-control.component.css']
})
export class HotelsControlComponent implements OnInit {
  hotels: HotelResult[];
  constructor(private hotelService: HotelService,
              private alertService: AlertService,
              private router: Router) { }

  ngOnInit() {
    this.getOwnHotels();
  }

  changeHotelInfo(id: string) {
    this.router.navigate([`/hotelcontrol/${id}`]);
  }

  showRooms(id: string) {
    this.router.navigate([`/rooms/${id}`]);
  }

  addRoom(id: string) {
    this.router.navigate([`/addRoom/${id}`]);
  }

  deleteHotel(id: string) {
    this.hotelService.deleteHotel(id)
      .pipe(first())
      .subscribe(
        () => {
          this.alertService.success('Delete successful', true);
          window.location.reload();
        },
        error => {
          this.alertService.error(error);
        });
  }

  private getOwnHotels() {
    this.hotelService.getUserHotels().pipe(first()).subscribe(hotels => {
      this.hotels = hotels;
    });
  }
}
