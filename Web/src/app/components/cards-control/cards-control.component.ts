import { Component, OnInit } from '@angular/core';
import {first} from 'rxjs/operators';
import {CardService} from '../../_services/card.service';
import {AlertService} from '../../_services/alert.service';
import {Card} from '../../Modules/card';

@Component({
  selector: 'app-cards-control',
  templateUrl: './cards-control.component.html',
  styleUrls: ['./cards-control.component.css']
})
export class CardsControlComponent implements OnInit {
  cards: Card[];
  constructor(
    private cardService: CardService,
    private alertService: AlertService) { }

  ngOnInit() {
    this.getCards();
  }

  deleteCard(id: string) {
    this.cardService.deleteCard(id)
      .pipe(first())
      .subscribe(
        () => {
          this.alertService.success('Delete successful', true);
          window.location.reload();
        },
        error => {
          this.alertService.error(error);
        });
  }

  getCards() {
    this.cardService.getCards().pipe(first()).subscribe(cards => {
      this.cards = cards;
    });
  }

}
