import { Component, OnInit } from '@angular/core';
import {RoomService} from '../../_services/room.service';
import {AlertService} from '../../_services/alert.service';
import {ActivatedRoute, Router} from '@angular/router';
import {RoomResult} from '../../Modules/room-result';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.component.html',
  styleUrls: ['./bookings.component.css']
})
export class BookingsComponent implements OnInit {

  bookings: any;

  constructor(private roomService: RoomService,
              private alertService: AlertService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.getBookings();
  }

  getBookings() {
    this.roomService.getBookings().pipe(first()).subscribe(bookings => {
      this.bookings = bookings;
    });
  }
}
