import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AlertService } from '../../_services/alert.service';
import { User } from '../../Modules/user';
import { UserService } from '../../_services/user.service';
import { HotelService } from '../../_services/hotel.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  profileForm: FormGroup;
  loading = false;
  submitted = false;
  currentUser: User;
  role = "";

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UserService,
    private hotelService: HotelService,
    private alertService: AlertService) { }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (this.currentUser.roleToAdd == 1) {
        this.role= "Tenant";
    } else {
      this.role = "Landlord";
    }
    this.profileForm = this.formBuilder.group({
      id: [this.currentUser.id],
      name: [this.currentUser.name, Validators.required],
      surName: [this.currentUser.surName, Validators.required],
      userName: [this.currentUser.userName, Validators.required],
      email: [this.currentUser.email, [Validators.required, Validators.email]],
      birthdayUtc: [this.currentUser.birthdayUtc, Validators.required],
      phoneNumber: [this.currentUser.phoneNumber, Validators.required],
      roleToAdd: [this.role, Validators.required]
    });
  }

  get f() { return this.profileForm.controls; }

  ShowHotels() {
    this.router.navigate(['/hotels']);
  }

  ShowCards() {
    this.router.navigate(['/cards']);
  }

  ChangePassword() {
    this.router.navigate(['/changePassword']);
  }

  addHotel() {
    this.router.navigate(['/addHotel']);
  }

  bookings() {
    this.router.navigate(['/bookings'])
  }

  addCard() {
    this.router.navigate(['/addCard']);
  }
  onSubmit() {
    this.submitted = true;

    if (this.profileForm.invalid) {
      return;
    }
    this.loading = true;
    this.userService.update(this.profileForm.value)
      .pipe(first())
      .subscribe(
        user => {
          this.currentUser.name = user.name;
          this.currentUser.phoneNumber = user.phoneNumber;
          this.currentUser.birthdayUtc = user.birthdayUtc;
          this.currentUser.surName = user.surName;
          this.currentUser.userName = user.userName;
          this.currentUser.email = user.email;
          localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
          this.alertService.success('Profile changed successful', true);
          window.location.reload();
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }
}
