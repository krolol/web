import { Component, OnInit } from '@angular/core';
import {first} from 'rxjs/operators';
import {HotelService} from '../../_services/hotel.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HotelResult} from '../../Modules/hotel-result';
import {ActivatedRoute} from '@angular/router';
import {environment} from '../../../environments/environment';
import {AlertService} from '../../_services/alert.service';

@Component({
  selector: 'app-search-hotel',
  templateUrl: './search-hotel.component.html',
  styleUrls: ['./search-hotel.component.css']
})
export class SearchHotelComponent implements OnInit {

  searchForm: FormGroup;
  hotels: HotelResult[];
  city: string;
  roomsCount: string;
  children: string;
  adults: string;
  dateFrom: string;
  dateTo: string;
  private sub: any;
  constructor(private hotelService: HotelService,
              private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private alertService: AlertService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.city = params['city'];
      environment.filter.city = this.city;
      this.roomsCount = params['roomsCount'];
      environment.filter.roomsCount = parseInt(this.roomsCount);
      this.children = params['children'];
      environment.filter.children = parseInt(this.children);
      this.adults = params['adults'];
      environment.filter.adults = parseInt(this.adults);
      this.dateFrom = params['dateFrom'];
      environment.filter.dateFrom = new Date(this.dateFrom).toISOString();
      this.dateTo = params['dateTo'];
      environment.filter.dateTo = new Date(this.dateTo).toISOString();
    });
    this.searchForm = this.formBuilder.group({
      city: [this.city, Validators.required],
      roomsCount: [this.roomsCount, Validators.required],
      children: [this.children, Validators.required],
      adults: [this.adults, Validators.required],
      dateFrom: [this.dateFrom, Validators.required],
      dateTo: [this.dateTo, Validators.required]
    });
    this.searchHotels();
  }

  private searchHotels() {
    this.hotelService.getHotelsByFilter(this.searchForm.value).pipe(first()).subscribe( hotels => {
      this.hotels = hotels;
      if (this.hotels.length === 0 || this.hotels == null) {
        this.alertService.error("Hotels not found");
      }
    });
  }
}
