import { Component, OnInit } from '@angular/core';
import {first} from 'rxjs/operators';
import {HotelService} from '../../_services/hotel.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder} from '@angular/forms';
import {Hotel} from '../../Modules/hotel';
import {HotelResult} from '../../Modules/hotel-result';
import {ReportResult} from '../../Modules/report-result';

@Component({
  selector: 'app-hotel',
  templateUrl: './hotel.component.html',
  styleUrls: ['./hotel.component.css']
})
export class HotelComponent implements OnInit {
  id: string;
  hotel: HotelResult;
  private sub: any;
  reports: ReportResult[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private hotelService: HotelService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
    });
    this.getHotel();
    this.getReports();
  }

  private getHotel() {
    this.hotelService.getHotel(this.id).pipe(first()).subscribe(hotel => {
      this.hotel = hotel;
    });
  }

  back() {
    this.router.navigate(['']);
  }

  addReport(id: string) {
    this.router.navigate([`hotelReport/${id}`]);
  }

  showRooms(id: string) {
    this.router.navigate([`hotelRooms/${id}`]);
  }

  private getReports() {
    this.hotelService.getReports(this.id).pipe(first()).subscribe(reports => {
      this.reports = reports;
    });
  }
}
