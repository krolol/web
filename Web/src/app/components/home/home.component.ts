import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertService} from '../../_services/alert.service';
import {User} from '../../Modules/user';
import {HotelService} from '../../_services/hotel.service';
import {HotelResult} from '../../Modules/hotel-result';
import {first} from 'rxjs/operators';
import {Router} from '@angular/router';

// @ts-ignore
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
  currentUser: User;
  login = false;
  searchForm: FormGroup;
  hotels: HotelResult[] = [];
  submitted = false;
  loading = false;
  dateTo: Date;
  dateFrom: Date;
  minDateTo = new Date(2019, 0, 2);
  constructor(
    private formBuilder: FormBuilder,
    private alertService: AlertService,
    private router: Router,
    private hotelService: HotelService) { }

  ngOnInit(): void {
    this.searchForm = this.formBuilder.group({
      city: ['Minsk', Validators.required],
      roomsCount: ['', Validators.required],
      children: ['', Validators.required],
      adults: ['', Validators.required],
      dateFrom: ['', Validators.required],
      dateTo: ['', Validators.required]
    });

    this.dateTo = new Date(this.searchForm.controls.dateTo.toString());
    this.dateFrom = new Date(this.searchForm.controls.dateFrom.toString());
  }

  onSubmit() {
    debugger;
    this.submitted = true;
    if (this.searchForm.invalid) {
      return;
    }
    this.loading = true;
    this.router.navigate([`/search/${this.searchForm.value.city}/${this.searchForm.value.roomsCount}/${this.searchForm.value.adults}/${this.searchForm.value.children}/${new Date(this.searchForm.value.dateFrom).toISOString()}/${this.searchForm.value.dateTo}`]);
  }
}
