import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatDatepickerModule } from '@angular/material/datepicker';
import {MatNativeDateModule, MatFormFieldModule, MatSelectModule, MatInputModule } from '@angular/material';

import { AppComponent } from './app.component';
import { routing } from './app-routing.module';

import { AlertComponent } from './components/alert/alert.component';
import { AuthGuard } from './_guard/auth.guard';
import { JwtInterceptor } from './_helper/jwt.interceptor';
import { ErrorInterceptor } from './_helper/error.interceptor';
import { AlertService } from './_services/alert.service';
import { UserService } from './_services/user.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RegistrationComponent } from './components/registration/registration.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatCardModule} from '@angular/material/card';
import { LoginComponent } from './components/login/login.component';
import { ProfileComponent } from './components/profile/profile.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { HotelsControlComponent } from './components/hotels-control/hotels-control.component';
import { CardsControlComponent } from './components/cards-control/cards-control.component';
import { HotelControlInfoComponent } from './components/hotel-control-info/hotel-control-info.component';
import { RoomsControlComponent } from './components/rooms-control/rooms-control.component';
import { AddHotelComponent } from './components/add-hotel/add-hotel.component';
import { AddRoomComponent } from './components/add-room/add-room.component';
import { AddCardComponent } from './components/add-card/add-card.component';
import {ImageUploadModule} from 'angular2-image-upload';
import { RoomControlInfoComponent } from './components/room-control-info/room-control-info.component';
import { HomeComponent } from './components/home/home.component';
import { HotelComponent } from './components/hotel/hotel.component';
import { RoomsComponent } from './components/rooms/rooms.component';
import { OpeartionCardComponent } from './components/opeartion-card/opeartion-card.component';
import { ReportComponent } from './components/report/report.component';
import { SearchHotelComponent } from './components/search-hotel/search-hotel.component';
import { BookingsComponent } from './components/bookings/bookings.component';

@NgModule({
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatSidenavModule,
    MatCardModule,
    ImageUploadModule.forRoot(),
    HttpClientModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatInputModule,
    MatSelectModule,

    routing,
    BrowserAnimationsModule
  ],
  declarations: [
    AppComponent,
    AlertComponent,
    RegistrationComponent,
    LoginComponent,
    ProfileComponent,
    ChangePasswordComponent,
    HotelsControlComponent,
    CardsControlComponent,
    HotelControlInfoComponent,
    RoomsControlComponent,
    AddHotelComponent,
    AddRoomComponent,
    AddCardComponent,
    RoomControlInfoComponent,
    HomeComponent,
    HotelComponent,
    RoomsComponent,
    OpeartionCardComponent,
    ReportComponent,
    SearchHotelComponent,
    BookingsComponent
  ],
  providers: [
    AuthGuard,
    AlertService,
    UserService,
    MatDatepickerModule,
    MatFormFieldModule,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
